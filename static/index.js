var outputCursor = "";
// 1111

$(function() {
    $("#cmd").change(cmdChanged);
    pollServer();
});

function pollServer() {
    $.ajax({ 
        url: "output",
        method: "GET",
        dataType: "json",
        data: {cursor: outputCursor },
        success: function(data) { 
            if(data.messages.length > 0) {
                data.messages.map(function(msg) {
                    outputCursor = msg.id;
                    var output = $("#output");
                    var content = document.createTextNode(msg.body);
                    output.append(content);
                    output.append(document.createElement("br"));
                    output.scrollTop(output.prop("scrollHeight"));
                });
            }
        },
        complete: pollServer,
        timeout: 30000
    });
}

function cmdChanged(evtArgs) {
    var command = $("#cmd").val();
    $.ajax( {
            url: "execute",
            data: {body: command},
            dataType: "json",
            method: "POST"
        }
    );
    $("#cmd").val("");
}

